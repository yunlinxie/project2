package edu.sjsu.android.project2yunlinxie;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    String data1[],data2[];
    int images[];
    Context context;
    public MyAdapter(Context ct, String s1[], int img[]){
        context = ct;
        data1 = s1;
        data2 = new String[]{"Facebook is one of the flag...",
                "Linkedin is one of the flag...",
                "Amazon is one of the flag...",
                "Apple is one of the flag...",
                "Google is one of the flag..."};
        images = img;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.my_row,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.myText1.setText(data1[position]);
        holder.myImage.setImageResource(images[position]);


        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(position == 4){
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Warning");
                    builder.setMessage("Warning: if Click on the last item Please continue " );
                    builder.setPositiveButton("Yes", (dialog, id) -> {
                        Intent intent = new Intent(context,DetailActivity.class);
                        intent.putExtra("data1",data1[position]);
                        intent.putExtra("data2",data2[position]);
                        intent.putExtra("myImage",images[position]);
                        context.startActivity(intent);
                    });
                    builder.setNegativeButton("No", (dialog, id) -> {});
                    builder.create().show();
                }else{
                    Intent intent = new Intent(context,DetailActivity.class);
                    intent.putExtra("data1",data1[position]);
                    intent.putExtra("data2",data2[position]);
                    intent.putExtra("myImage",images[position]);
                    context.startActivity(intent);
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView myText1,myText2;
        ImageView myImage;
        ConstraintLayout mainLayout;

        //RecyclerView recyclerView;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            myText1 = itemView.findViewById(R.id.myText1);
            myImage = itemView.findViewById(R.id.myImageView);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            //recyclerView = itemView.findViewById(R.id.recyclerView);
        }
    }
}