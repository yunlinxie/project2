package edu.sjsu.android.project2yunlinxie;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    String s1[];

    int images[] ={R.drawable.facebook,
                    R.drawable.linkedin,
                    R.drawable.amazon,
                    R.drawable.apple,
                    R.drawable.google};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);

        s1 = getResources().getStringArray(R.array.Tech_Companies);

        MyAdapter myAdapter = new MyAdapter(this, s1, images);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ActionBar action = getSupportActionBar();
        action.setTitle("My Directory");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.information:
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra("name", "Yunlin Xie's Dream List");
                intent.putExtra("phone", "888-8888");
                startActivity(intent);
                return true;
            case R.id.Uninstall:
                Intent delete = new Intent(Intent.ACTION_DELETE, Uri.parse("package:" + getPackageName()));
                startActivity(delete);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}